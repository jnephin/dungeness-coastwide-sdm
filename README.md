# Dungeness Crab Integrated SDM

__Main author:__  Jessica Nephin    
__Contributors:__ Patrick L. Thompson, Sean C. Anderson, Ashley E. Park, Christopher N. Rooper, Brendan Aulthouse, and Joe Watson    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: jessica.nephin@dfo-mpo.gc.ca


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [References](#references)


## Objective
Predicting Dungeness Crab distribution in British Columbia by integrating survey data and independently evaluating with fisheries catch


## Summary
Develop and evaluate six candidate generalized linear mixed-effect models (GLMMs) with spatial random fields from dive, trawl, and trap survey data.


## Status
Completed


## Contents
This repository includes python code as a Jupyter notebook for processing fisheries catch data, R code for species distribution modelling and evaluation, and Latex code for document creation.

## Methods
Species distribution modelling performed with sdmTMB R package. For full methods refer to publication.

## Requirements
R version 4.0.2    
Python 3    
Latex    


## Caveats
This repository was created to share code associated with a publication. To be used for another purpose with different datasets, it will need to be adapted.


## References
