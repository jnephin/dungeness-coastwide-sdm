# Evaluate predictions with mean catch from fisheries logs

# packages
require(raster)
require(terra)
require(ggplot2)
require(dplyr)
require(rgdal)
require(classInt)
require(ModelMetrics)
require(reshape2)
require(viridisLite)
require(patchwork)

# Parent Directory
setwd('..')

# projects
projects <- c("Trap-DC", "Gear-effect","Survey-effect")

# Model type palette 
pal <- c("#9553AB", "#D55E00", "#02598A")

# levels for plots
lvls <- c("Trap-DC",  "Gear-effect",  "Survey-effect")


#------------------------------------------------------------------------------#
# Load catch raster

catch <- rast("Catch/Dungy_CPUE_15kmSmooth_Clipped.tif")
plot(catch)



#------------------------------------------------------------------------------#
# Get predictions

# Empty objects to fill
preds <- list()
preds_min <- list()
preds_max <- list()

# loop through projects 
for (p in projects){
    
    # get raster pred
    if( grepl("Survey-effect", p) ){
      tmp <- rast( file.path("Model", p, "Data", 
                             paste0("Prediction_Trap_DC.tif")) )
      tmp_min <- rast( file.path("Model", p, "Data", 
                             paste0("Min_Trap_DC.tif")) )
      tmp_max <- rast( file.path("Model", p, "Data", 
                             paste0("Max_Trap_DC.tif")) )
    } else if( grepl("Gear-effect", p) ){
      tmp <- rast( file.path("Model", p, "Data", 
                             paste0("Prediction_Trap.tif")) )
      tmp_min <- rast( file.path("Model", p, "Data", 
                             paste0("Min_Trap.tif")) )
      tmp_max <- rast( file.path("Model", p, "Data", 
                             paste0("Max_Trap.tif")) )
    } else {
      tmp <- rast( file.path("Model", p, "Data", 
                             paste0("Prediction.tif")) )
      tmp_min <- rast( file.path("Model", p, "Data", 
                             paste0("Min.tif")) )
      tmp_max <- rast( file.path("Model", p, "Data", 
                             paste0("Max.tif")) )
    }
    names(tmp) <- p
    preds[[p]] <- tmp
    names(tmp_min) <- p
    preds_min[[p]] <- tmp_min
    names(tmp_max) <- p
    preds_max[[p]] <- tmp_max

}
# Convert to terra layers
m <- rast(preds)
m_min <- rast(preds_min)
m_max <- rast(preds_max)



#------------------------------------------------------------------------------#
# Compare distribution of predictions in and outside cpue area

# catch mask
inmask <- resample(catch, m)
inmask[inmask > -Inf] <- 1
outmask <- inmask
outmask[is.na(outmask)] <- 0
outmask[outmask == 1 ] <- NA
outmask[outmask == 0 ] <- 1
plot(inmask)
plot(outmask)


# Get in and out values  
dat <- NULL
for (i in 1:nlyr(m)){
  # mask
  inm <- terra::values(m[[i]] * inmask, mat=F) %>% na.omit()
  outm <- terra::values(m[[i]] * outmask, mat=F) %>% na.omit()
  tmp <- data.frame( layer=rep(names(m)[[i]], length(inm)+length(outm)), 
                     type=c(rep("In", length(inm)), rep("Out", length(outm))), 
                     values=c(inm,outm) )
  dat <- rbind(dat, tmp)
}


# calculate suitable habitat area within CPUE and overall

# Get threshold values
threshs <- NULL
# loop through projects
for (p in projects){
  # locations
  loc <- file.path("Model", p, "Data")
  # threshold table
  th <- read.csv(file.path(loc, "threshold.csv"))
  th$Model <- p
  # Add
  threshs <- rbind( threshs, th )
}
# Subset
thresholds <- c("MaxSens+Spec","MaxKappa","MaxPCC")
threshs <- threshs[threshs$Method %in% thresholds,]
# Select threshold for appropriate predictions
ss <- which(threshs$Model %in% c('Dive', 'Trawl', 'Trap-GC','Trap-DC'))
tp <- which(threshs$Model == 'Trap' & threshs$Type == 'Trap')
ge <- which(threshs$Model == 'Gear-effect' & threshs$Type == 'Trap')
se <- which(threshs$Model == 'Survey-effect' & threshs$Type == 'Trap_DC')
al <- which(threshs$Model == 'All' & threshs$Type == 'Dive|Trawl|Trap_DC|Trap_GC')
thresh <- threshs[c(ss, tp, ge, se, al),]
# Mean threshold
mthresh <- aggregate(Predicted ~ Model, mean, dat=thresh)
mthresh$Predicted <- round(mthresh$Predicted, 2)
# All
allthresh <- thresh[thresh$Model %in% lvls, ]

# Loop through thresholds to calculate PCC
pcc <- NULL
harea <- NULL
for (i in  unique(allthresh$Method)){
  
  # select threshold 
  thresh <- allthresh[allthresh$Method == i,]
  th <- thresh$Predicted
  names(th) <- thresh$Model
  
  # Classify
  dat$threshold <- th[dat$layer]
  dat$suitable <- "no"
  dat$suitable[dat$values >= dat$threshold] <- "yes"
  dat$area <- 0.25 # 0.25 squared kilometers
  dat$correct <- "no"
  dat$correct[dat$suitable == "yes" & dat$type == "In"] <- "yes"
  dat$correct[dat$suitable == "no" & dat$type == "Out"] <- "yes"
  
  # habitat area total
  hareatotal <- aggregate(area ~ layer, sum, data=dat[dat$suitable == "yes",])
  hareatotal$area <- hareatotal$area
  hareatotal$Method <- i
  
  # proportion of correctly classified
  carea <- aggregate(area ~ layer + correct, sum, data=dat)
  propc <- round(carea$area[carea$correct == "yes"] / 
                   carea$area[carea$correct == "no"], 1)
  names(propc) <- hareatotal$layer
  dp <- data.frame(r=sort(propc, decreasing = T))
  dp$Model <- row.names(dp)
  dp$Method <- i
  
  # Bind
  pcc <- rbind(pcc, dp)
  harea <- rbind(harea, hareatotal)
}

# Calculate mean and se
mpcc <-  pcc %>% group_by(Model) %>%
  summarize(Mean = mean(r),
            SE = sd(r)/sqrt(length(r))) %>% as.data.frame()
mharea <-  harea %>% group_by(layer) %>%
  summarize(Mean = mean(area),
            SE = sd(area)/sqrt(length(area))) %>% as.data.frame()
mharea$CI <- 2 * mharea$SE
mpcc$CI <- 2 * mpcc$SE
mharea[order(mharea$Mean, decreasing = T),]
#           layer     Mean       SE        CI
# 1   Gear-effect 18292.00 2695.424  5390.848
# 2 Survey-effect 43225.75 6222.500 12445.000
# 3       Trap-DC 82217.42 5503.833 11007.667
mpcc[order(mpcc$Mean, decreasing = T),]
#            Model      Mean        SE        CI
# 1   Gear-effect 2.9666667 0.1333333 0.2666667
# 2 Survey-effect 2.7000000 0.3000000 0.6000000
# 3       Trap-DC 0.9333333 0.1333333 0.2666667

# labels
dlabs <- dat[!duplicated(dat$layer),]
dlabs <- merge(dlabs, mpcc, by.x='layer', by.y="Model")
dlabs$Mean <- round(dlabs$Mean, 1)
dlabs$SE <- round(dlabs$SE, 2)
dlabs


# levels
dat$Model <- factor(dat$layer, levels = lvls)
allthresh$Model <- factor(allthresh$Model, levels = lvls)
#allthresh[5,2] <- allthresh[5,2] + .005
dlabs$Model <- factor(dlabs$layer, levels = lvls)



# CPUE Map
# get template raster for study area
load("DataPrep/Data/EnviroLayers_Dataframe.RData")
base <- rast("DataPrep/Data/Template_Raster.tif")
load("DataPrep/Data/MCPolygon.RData")
load( file="DataPrep/Data/LandPolygon.RData")


# Calculate the x,y extents for all maps
base[env_df$ID] <- env_df$Depth
base[base > -Inf] <- 1

base_pixel <- as(as(base, "Raster"), "SpatialPixelsDataFrame")
base_df <- as.data.frame(base_pixel)
names(base_df)[1] <- "value"
base_df$label <- "Out"

# scale 0 to 1
cs <- catch / minmax(catch)[2]

# convert raster for ggplot
layer_pixel <- as(as(cs, "Raster"), "SpatialPixelsDataFrame")
layer_df <- as.data.frame(layer_pixel)
names(layer_df)[1] <- "value"
f_df <- layer_df
f_df$value[f_df$value > 0] <- 1
f_df$label <- "In"

# Get the vertical and horizontal limits
load("DataPrep/Data/EnviroLayers_Dataframe.RData")
base <- raster("DataPrep/Data/Template_Raster.tif")
# Calculate the x,y extents for all Figures
base[env_df$ID] <- env_df$Depth
# convert raster for extents
ras_spdf <- as( base, "SpatialPixelsDataFrame" )
# Get the vertical and horizontal limits
ext <- extent( ras_spdf )
# Get x and y limits
adj <- round( ext@xmin*0.01 )
lims <- list( x=c(ext@xmin-adj, ext@xmax+adj), y=c(ext@ymin-adj, ext@ymax+adj) )


# set legend colourbar
gbar <- guide_colorbar( frame.colour = "black", ticks.colour="black",
                        barheight = .5, barwidth = 5.2,
                        title.position="top", title.hjust = 0.5,
                        col = guide_legend(order = 2))
# Set scale
mpal <-rev(magma(8))
# Add point for legend
l <- data.frame(x=1000000, y=100000,label='Prediction extent')
# add text
labs <- data.frame(x=c(1150000, 610000, 720000, 1000000),
                     y=c(480000, 980000, 1070000, 450000),
                     label=c('Vancouver',
                        'Dogfish\nBank',
                        'Chatham\nSound',
                        'Clayoquot\nSound'))


# Make the cpue hotspot map
cMap <- ggplot( data=NULL) +
  geom_point(data=l, aes(x=x, y=y, col=label), shape=15, size=5)+
  geom_tile( data=base_df, aes(x=x, y=y), fill="#B2CBD8" ) +
  geom_tile( data=layer_df, aes(x=x, y=y, fill=value) ) +
  geom_polygon( data=land$bcDF, aes(x=Longitude, y=Latitude, group=group),
                fill="grey80", colour="grey60", size=0.01 ) +
  geom_text(data=labs, aes(x=x, y=y, label=label), size=2.5, col='#00384d')+
  scale_fill_gradientn( name="CPUE Index", colours=mpal,
                        guide = gbar, na.value = "white",
                        breaks=c(0.2,0.4,0.6,0.8)) +
  scale_color_manual("", values="#92b5c8") + 
  xlab( expression(paste("Eastings (", km, ")")) ) +
  ylab( expression(paste("Northings (", km, ")")) ) +
  theme_bw( ) +
  coord_equal( xlim=lims$x, ylim=lims$y ) +
  scale_x_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  scale_y_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  theme( legend.box.just=0, legend.direction = "horizontal",
         legend.background=element_blank(),
         axis.text = element_text(size=8),
         axis.title = element_text(size=9),
         legend.text = element_text(size=8),
         legend.title = element_text(size=8),
         panel.border=element_rect(colour="black"),
         legend.spacing.y = unit(.1, "cm"),
         legend.margin = margin(0, .2, .2, .2, "cm"),
         panel.grid=element_blank(),
         legend.justification=c(0, 0),
         legend.position=c(0.01, 0))
         #plot.margin = margin(.2, .1, 0, .2, "cm"))
cMap
# # Save the plot
# ggsave( plot=gMap, height=5, width=4.4,
#         filename=paste0("Paper/Figures/","Map_CPUE.png"))


# Make the cpue footprint map
fMap <- ggplot( data=NULL) +
  geom_tile( data=base_df, aes(x=x, y=y, fill=label) ) +
  geom_tile( data=f_df, aes(x=x, y=y, fill=label) ) +
  geom_polygon( data=land$bcDF, aes(x=Longitude, y=Latitude, group=group),
                fill="grey80", colour="grey60", size=0.01 ) +
  scale_fill_manual("Fishery footprint", values = c("#edd191", "#92b5c8"),
                    guide=guide_legend(title.position="top")) +
  xlab( expression(paste("Eastings (", km, ")")) ) +
  ylab( expression(paste("Northings (", km, ")")) ) +
  theme_bw( ) +
  coord_equal( xlim=lims$x, ylim=lims$y ) +
  scale_x_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  scale_y_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  theme( legend.box.just=0, 
         legend.direction = "horizontal",
         legend.background=element_blank(),
         axis.text = element_text(size=8),
         axis.title = element_text(size=9),
         legend.text = element_text(size=8),
         legend.title = element_text(size=8),
         panel.border=element_rect(colour="black"),
         panel.grid=element_blank(),
         legend.justification=c(0, 0),
         legend.position=c(0.01, 0),
         plot.margin = margin(.2, .1, 0, .2, "cm"))
fMap


# plot in and out footprint density
dPlot <- ggplot( ) +
  geom_density(data=dat, aes(y=values, colour=type, fill=type), 
               adjust=3, alpha=0.3) +
  scale_fill_manual("", values = c("#E69F00", "#02598A"), guide="none") +
  scale_colour_manual("", values = c("#E69F00", "#02598A"), guide="none") +
  geom_hline(data=allthresh, aes(yintercept=Predicted, group=factor(Method)), 
             linetype='dashed', col="grey50", size=.5)+
  geom_text(data=dlabs, aes(label = paste0(Model)), 
            y = 0.02, x = Inf,  hjust=0, vjust=1.5, size=2.8) +
  facet_wrap(~ Model, ncol=1, scales = "free_y") +
  coord_flip( ) +
  theme_bw( ) +
  scale_x_continuous(expand = expansion(mult = c(0, .2))) +
  scale_y_continuous(limits=c(-0.02,1.02), expand = expansion(mult = c(0, 0)), 
                     breaks = c(0, 0.2, 0.4, 0.6, 0.8, 1)) +
  ylab( "Probability of detection" ) +
  theme( axis.title.y = element_blank(),
         axis.text.y = element_blank(),
         axis.ticks.y = element_blank(),
         plot.background = element_blank(),
         panel.background = element_blank(),
         strip.background = element_blank(),
         strip.text = element_blank(),
         axis.text = element_text(size=8),
         axis.title = element_text(size=9),
         legend.text = element_text(size=8),
         legend.position = c(0.9,0.95),
         legend.direction = "vertical",
         legend.background = element_blank(),
         legend.key.size = unit(.4, units="cm"),
         panel.spacing = unit(.1, units="cm"),
         panel.border=element_rect(colour="black"),
         panel.grid = element_blank(),
         plot.margin = margin(.2, .2, 0, .1, "cm"))
dPlot
# # Save the plot
# ggsave( plot=dPlot, height=6, width=5.5, 
#         filename=paste0("Paper/Figures/","InOut_Density.png"))
# 

## Combine footprint map and in/out density into one figure
# Arrange the side by side using patchwork package
cPlots <- fMap + dPlot + plot_layout(widths = c(3, 2))

# Save the plot
ggsave( plot=cPlots, height=3.8, width=6,
        filename=paste0("Paper/Figures/","Map_InOutDensity.png"))




#------------------------------------------------------------------------------#
# Calculate hotspots and overlap

# path to Inputs
indir <- file.path("DataPrep/Data")
# Load base raster for predictions
r <- rast( file.path(indir, "Template_Raster.tif") )
# Load enviro data for predictions to new areas
load( file=file.path(indir, "EnviroLayers_Dataframe.RData") ) # env_df

# ID raster and values
r[env_df$ID] <- env_df$ID
id_vals <- terra::values(r, dataframe=TRUE)[[1]]

# Match IDs for r and catch (needed for overlap)
# Remove cpue outside the study area (is.na(ID))
r_catch <- resample(catch, r)
c_vals <- terra::values(r_catch, dataframe=TRUE)[[1]]
c_vals <- c_vals[!is.na(id_vals)]

# Preds dataframe
p_vals <- terra::values(m, dataframe=TRUE)
p_vals <- p_vals[!is.na(id_vals),]
p_min <- terra::values(m_min, dataframe=TRUE)
p_min <- p_min[!is.na(id_vals),]
p_max <- terra::values(m_max, dataframe=TRUE)
p_max <- p_max[!is.na(id_vals),]

# Add control
p_vals$Control <- 1
p_min$Control <- 1
p_max$Control <- 1

# # Examine
# rdat <- r
# rdat[env_df$ID] <- p_max[,3]
# plot(rdat)
# catch8 <- r_catch
# brkpnt <- quantile( terra::values(catch8), .95, na.rm=T )
# catch8[catch8 < brkpnt] <- 0 
# catch8[catch8 >= brkpnt] <- 1 
# trap8 <- m_max[[1]]
# gear8 <- m_max[[2]]
# survey8 <- m_max[[3]]
# brkpnt <- t(apply(terra::values(m), 2, function(x) quantile(x, .95, na.rm=T)))
# trap8 [trap8  < brkpnt[[1]]] <- 0
# trap8 [trap8  >= brkpnt[[1]]] <- 1
# gear8  [gear8   < brkpnt[[2]]] <- 0
# gear8  [gear8   >= brkpnt[[2]]] <- 1
# survey8[survey8   < brkpnt[[3]]] <- 0
# survey8[survey8   >= brkpnt[[3]]] <- 1
# plot(c(trap8,gear8,survey8,catch8))



# Hotspot function
hotspots <- function(level, cdat, pdat, pmin, pmax){
  # For predictions
  # Number of cells for prediction hotspot at level
  h_cells <- nrow(pdat) - floor(quantile(1:nrow(pdat), level, na.rm=T))[[1]]
  # Break points 
  brkpnt <- t(apply(pdat, 2, function(x) quantile(x, level, na.rm=T)))
  brkpnt <- do.call("rbind", replicate(nrow(pdat), brkpnt, simplify = FALSE))
  brkpnt_min <- t(apply(pmin, 2, function(x) quantile(x, level, na.rm=T)))
  brkpnt_min <- do.call("rbind", replicate(nrow(pmin), brkpnt_min, simplify = FALSE))
  brkpnt_max <- t(apply(pmax, 2, function(x) quantile(x, level, na.rm=T)))
  brkpnt_max <- do.call("rbind", replicate(nrow(pmax), brkpnt_max, simplify = FALSE))
  # Bin
  pdat[pdat < brkpnt] <- 0 
  pdat[pdat >= brkpnt] <- 1
  pmin[pmin < brkpnt_min] <- 0 
  pmin[pmin >= brkpnt_min] <- 1
  pmax[pmax < brkpnt_max] <- 0 
  pmax[pmax >= brkpnt_max] <- 1
  # Subsample hotspots if greater than h_cells
  # Number of hotspots need to be consistent for comparison between models
  # If model predictions have many values == 1, then hotspot quantiles would have too many cells
  # Loop through layers
  for (i in 1:(ncol(pdat)) ){
    if( length(pdat[,i][pdat[,i] == 1]) > h_cells ){
      # Which indicies are 1
      inds <- which(pdat[,i] == 1)
      pmin_inds <- which(pmin[,i] == 1)
      pmax_inds <- which(pmax[,i] == 1)
      # Sample indices, to get the correct number
      zeroinds <- sample(x=inds, size=length(inds)-h_cells)
      pmin_zeroinds <- sample(x=pmin_inds, size=length(pmin_inds)-h_cells)
      pmax_zeroinds <- sample(x=pmax_inds, size=length(pmax_inds)-h_cells)
      # Assign samples indicies to zero
      pdat[zeroinds,i] <- 0
      pmin[pmin_zeroinds,i] <- 0
      pmax[pmax_zeroinds,i] <- 0
    }
  }
  # For catch
  # Break points
  brkpnt <- quantile( cdat, level, na.rm=T )
  # Bin
  cdat[cdat < brkpnt] <- 0 
  cdat[cdat >= brkpnt] <- 1 
  # Hotspot overlap
  ov <-  cdat + pdat #[,1:(ncol(pdat)-1)]
  ov_min <-  cdat + pmin #[,1:(ncol(pmin)-1)]
  ov_max <-  cdat + pmax #[,1:(ncol(pmax)-1)]
  # Bind
  ovb <- cbind(ov, ov_min, ov_max)
  pall <- cbind(pdat, pmin, pmax)
  # Number of hotspot overlap cells
  ncells <- apply(ovb, 2, function(x) length(x[x == 2 & !is.na(x)]))
  # Number of all catch hotspot cells
  ntotal_preds <- apply(pall, 2, function(x) length(x[x == 1 & !is.na(x)]))
  ntotal_catch <-length(cdat[cdat == 1 & !is.na(cdat)])
  #Percent
  povp <- ncells / ntotal_preds * 100
  povc <- ncells / ntotal_catch * 100
  pov <- (povp + povc) /2
  # Add type
  pov <- data.frame(pp=povp, pc=povc, cb=pov, Model=names(pov),
                    Type=rep(c("y","min","max"), each=ncol(ov)))
  # return
  return(pov)
}


# Hotspot cut-offs
cutoffs <- seq(0.65, 0.95, by=0.01)
# Calculate hotspot overlap
ovdat <- lapply(cutoffs, FUN = hotspots, cdat=c_vals, pdat=p_vals, pmin=p_min, pmax=p_max)
# Reshape
ovmelt <- melt( ovdat, id.vars=c("Model", "Type"))
ovmelt$cutoff <- rep( cutoffs, each=length(projects)*4*3 )
ovm <- dcast(data=ovmelt, Model + cutoff + variable ~ Type, value.var="value" )
# levels
ovm$Model <- factor(ovm$Model, levels = c('Control',"Trap-DC","Gear-effect","Survey-effect"))

#plot
bPlot <- ggplot( data=ovm[ovm$variable == 'cb' & ovm$Model != 'Control',], 
                 aes(x=cutoff, colour=Model) ) +
  #geom_ribbon(aes(ymin = min, ymax = max, fill = Model), col=NA, alpha=.3) +
  geom_line(aes(y=y ), size=1) +
  geom_vline(xintercept=0.9, linetype="dashed", col="grey50", size=.5)+
  scale_colour_manual( "", values = pal ) +
  scale_fill_manual( "", values = pal ) +
  scale_x_continuous(expand = expansion(mult = c(0, 0))) +
  ylab( "Hotspot Overlap (%)" ) +
  xlab( "Quantile" ) +
  theme_bw( ) +
  theme( panel.border=element_rect(colour="black"),
         panel.grid.major = element_blank(),
         panel.grid.minor = element_blank(),
         axis.text = element_text(size=8),
         axis.title = element_text(size=9),
         axis.ticks = element_line(size=.4),
         axis.ticks.length = unit(.1, units="cm"),
         legend.text = element_text(size=8),
         legend.position = c(0.25,0.27),
         legend.key.width = unit(.5, units="cm"),
         legend.title = element_blank(),
         legend.background = element_blank(),
         legend.key.size = unit(.7, units="cm"))
bPlot

# # Save the plot
# ggsave( plot=bPlot, height=2.8, width=5.2, 
#         filename=paste0("Paper/Figures/","Hotspot_Overlap.png"))

# Maximum hotspot overlap
ovm$cutoff[ovm$y == max(ovm$y)]
mh <- ovm[ovm$cutoff == .95,]
mh[order(mh$y, decreasing = T),]

# Mean hotspot overlap from 0.65 to 0.95
movm <- ovm %>% group_by(Model) %>%
  summarize(Mean = mean(y),
            SE = sd(y)/sqrt(length(y))) %>% as.data.frame()
movm$CI <- 2*movm$SE
movm$lowCI <- movm$Mean - movm$CI
movm$highCI <- movm$Mean + movm$CI
movm
#           Model     Mean        SE       CI    lowCI   highCI
# 1       Control 13.69283 0.8624514 1.724903 11.96793 15.41773
# 2       Trap-DC 44.25976 2.2018371 4.403674 39.85608 48.66343
# 3   Gear-effect 54.65709 2.2295483 4.459097 50.19799 59.11619
# 4 Survey-effect 49.86244 2.4589312 4.917862 44.94458 54.78030


#--------------------------------------------------------------------------#
# map predictions and hotspots combined

# Empty objects to fill
preds <- list()
# loop through projects 
for (p in projects){
  
  # get raster pred
  if( grepl("Survey-effect", p) ){
    tmp <- rast( file.path("Model", p, "Data", paste0("Prediction_Trap_DC.tif")) )
  } else if( grepl("Gear-effect", p) ){
    tmp <- rast( file.path("Model", p, "Data", paste0("Prediction_Trap.tif")) )
  } else {
    tmp <- rast( file.path("Model", p, "Data", paste0("Prediction.tif")) )
  }
  names(tmp) <- p
  preds[[p]] <- tmp
}
# Convert to terra layers
m <- rast(preds)

# Convert raster stack to dataframe for ggplot
p_df <- as.data.frame(as(stack(m), "SpatialPixelsDataFrame"))
p_melt <- melt(p_df, id.vars=c("x","y"))
names(p_melt) <- c("x","y","Model", "value")
p_melt$Model <- sub('[.]','-', p_melt$Model)

# levels
p_melt$Model <- factor(p_melt$Model, levels = lvls)

# labels
dlabs <- p_melt[!duplicated(p_melt$Model),]

# Hotspot polygon
MCP <- obsMCP[,1:2]
colnames(MCP) <- c("x","y")

# Palette
mpal <- rev(magma(8))

# Map legend name
predLegend <- "Probability of detection"

# Guide settings
gbar <- guide_colorbar( frame.colour = "black", ticks.colour="black",
                        barheight = 7, barwidth = .6,
                        title.position="right", title.hjust = 0.5)

# Quantile scale
qs <- p_melt %>% group_by(Model) %>%
  mutate(quantiles =  cut(value, unique(quantile(value, probs=seq(0, 1, length.out=8))))) %>%
  as.data.frame()
qs$quantile_values <- sub('.*,','', qs$quantiles)
p_melt$quantile_values <- as.numeric(sub(']','', qs$quantile_values))

# Make the map of continuous predictions
pMap <- ggplot( data=p_melt ) +
  geom_polygon( data=land$bcDF, aes(x=Longitude, y=Latitude, group=group),
                fill="grey80", colour=NA, size=0.01 ) +
  geom_tile( data=p_melt, aes(x=x, y=y, fill=quantile_values) ) +
  facet_wrap(~Model, ncol=1) + 
  geom_polygon( data=obsMCP,
                aes(x=long, y=lat, group=group),
                colour="#00FDFD", fill="#00FDFD", alpha=.2, size=.3 ) +
  geom_text(data=dlabs, aes(label = Model), 
            y = max(p_melt$y), x = max(p_melt$x)*.96, 
            hjust=1, vjust=2, size=3) +
  scale_fill_gradientn( limits=c(0,1), name=predLegend,
                        colours=mpal, guide = gbar, 
                        na.value = "white", 
                        breaks=c(0.2, 0.4, 0.6, 0.8) ) +
  theme_bw( ) +
  coord_equal( xlim=lims$x, ylim=lims$y ) +
  scale_x_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  scale_y_continuous( labels=function(x) x/1000, expand=c(0, 0)) +
  theme( legend.box.just=0, legend.direction = "vertical",
         legend.background=element_blank(),
         legend.text = element_text(size=8),
         legend.title = element_text(size=9, angle=270),
         axis.text = element_blank(),
         axis.title = element_blank(),
         axis.ticks = element_blank(),
         panel.border=element_rect(colour="black"),
         panel.grid=element_blank(),
         strip.background = element_blank(),
         strip.text = element_blank(),
         panel.spacing = unit(.1, units="cm"),
         legend.justification=c(0.5, 0.5),
         legend.position='right',
         legend.margin=margin(t = -0.1, unit='cm'))
pMap


## Combine footprint map and in/out density into one figure
# Arrange the side by side using patchwork package
hPlots <- cMap / bPlot +  plot_layout(heights = c(3, 2))
hhPlots <- hPlots - pMap +  plot_layout(widths = c(5, 3))
#hhPlots

# Save the plot
ggsave( plot=hhPlots, height=6, width=6,
        filename=paste0( "Paper/Figures/Map_Hotspots_Predictions.png" ) )





# # --------------------------------------------------------------------------
# # CPUE hotspot polygon
# 
# # Save CPUE hotspot raster (for 90 quantile cut off)
# # Break points
# brkpnt <- quantile( terra::values(catch), 0.90, na.rm=T )
# # Bin
# cb <- catch
# cb[cb < brkpnt] <- 0
# cb[cb >= brkpnt] <- 1
# plot(cb)
# cpuehs <- cb
# cpuehs[cpuehs == 0] <- NA
# plot(cpuehs)
# writeRaster(cpuehs, "Catch/Dungy_CPUE_Hotspots_90.tif", overwrite=TRUE)
# 
# # polygon footprint
# hsf <- rasterToPolygons(as(cpuehs, "Raster"), dissolve=T)
# hsp <- disaggregate(hsf)
# # remove really small polygons
# hsp$area_sqkm <- raster::area(hsp) / 1000000
# summary(hsp$area_sqkm)
# hss <- hsp[hsp$area_sqkm > 100,]
# # plot
# plot(hsp, col="blue", lwd=0.1)
# plot(hss, add=T, col="red", lwd=0.1)
# # write
# writeOGR(hss, dsn="Catch", layer="Dungy_CPUE_Hotspots_90_footprint",
#          driver="ESRI Shapefile", overwrite=TRUE)





